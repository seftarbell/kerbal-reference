'use strict';

/*
if(typeof angular == 'undefined') {
    //do something here
    console.log('Angular is busted!');
} else {console.log('Angular is working!')}
*/
// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);
